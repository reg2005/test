<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advert extends Model
{
    public $table = 'adverts';

    public $primaryKey = 'user_id';

    public $fillable = [];

    public $guarded = [];
}
