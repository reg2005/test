<?php

namespace App\Providers;

use App\Advert;
use App\Order;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Order::creating(function ($item) {
            $item->order_add_time = Carbon::now();
        });

        Advert::creating(function ($item) {
            $item->user_password = \Hash::make($item->user_password );
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
