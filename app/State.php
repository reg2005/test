<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public $table = 'states';

    public $primaryKey = 'state_id';

    public $fillable = [];

    public $guarded = [];
}
