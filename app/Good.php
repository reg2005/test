<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    public $table = 'goods';

    public $primaryKey = 'good_id';

    public $fillable = [];

    public $guarded = [];

    public function advert(){
        return $this->hasOne('App\Advert', 'user_id', 'good_advert');
    }
}
