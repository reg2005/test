<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $table = 'orders';

    public $primaryKey = 'order_id';

    public $fillable = [];

    public $guarded = [];



    public function state(){
        return $this->hasOne('App\State', 'state_id', 'order_state');
    }

    public function good(){
        return $this->hasOne('App\Good', 'good_id', 'order_good');
    }

    public function scopeWhereName($scope, $name = '')
    {
        return $scope->leftJoin(
            'goods',
            'orders.order_good', '=', 'goods.good_id'
        )->where('goods.good_name', 'like', "%{$name}%");
    }
}
