<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 04.07.16
 * Time: 12:10
 */

namespace App\Http\Controllers;
use App\User;
use Illuminate\Routing\Controller as BaseController;
use Yajra\Datatables\Facades\Datatables;


class AdvertController extends BaseController
{

    public function getList(){
        // Using Eloquent
        return view('datatables.advert');
    }

    public function getEditData($id = null){
        throw new \Exception('Тут должна быть форма редактирования, но в ТЗ её нет');
    }

    public function anyData()
    {
        return Datatables::of(User::query())
            ->make(true);
    }

}