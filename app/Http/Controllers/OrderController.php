<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 04.07.16
 * Time: 12:10
 */

namespace App\Http\Controllers;
use App\Order;
use App\State;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Yajra\Datatables\Facades\Datatables;


class OrderController extends BaseController
{

    public function getList(){
        // Using Eloquent
        $states = call_user_func(function(){
            $res[0] = 'Выберите статус';
            foreach(State::all() as $item){
                $res[$item->state_id] = $item->state_name;
            }
            return $res;
        });
        return view('datatables.order', ['states' => $states]);
    }

    public function getEditData($id = null){
        throw new \Exception();
    }

    public function anyData(Request $request)
    {
        $items = Order::query()->with('state')->with('good');
        return Datatables::of($items)
            ->filter(function ($query) use ($request) {
                if ($request->has('from')) {
                    $query->where('order_add_time', '>', new Carbon($request->get('from')) );
                }

                if ($request->has('to')) {
                    $query->where('order_add_time', '<', new Carbon($request->get('to')) );
                }

                if ($request->has('order_state') AND $request->get('order_state')) {
                    $query->where('order_state', $request->get('order_state') );
                }

                if ($request->has('order_client_phone')) {
                    $query->where('order_client_phone', 'like', "%{$request->get('order_client_phone')}%");
                }

                if ($request->has('good_name')) {
                    $query->whereName($request->get('good_name'));
                }

                if ($request->has('good_id')) {
                    $query->where('order_id', $request->get('good_id'));
                }
            })
            ->editColumn('good', function($row) {
                $row->good->advert();
                $adv = $row->good->advert;
                return '
                    <div><a href="'.route('good.edit', ['id' => $row->good->good_id]).'">'. $row->good->good_name .'</a></div>'.
                    $adv->user_first_name.' '.$adv->user_last_name.'( '.$adv->user_login.' )';
            })
            ->editColumn('state', function($row) {
                $color = call_user_func(function() use($row){
                    switch($row->state->state_slug){
                        case 'new':
                            return 'label-danger';
                        case 'accepted':
                            return 'label-success';
                        default:
                            return 'label-info';
                    }
                });


                return '<span class="label '.$color.'">'.$row->state->state_name.'</span>';
            })
            ->make(true);
    }

}