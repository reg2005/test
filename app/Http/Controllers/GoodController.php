<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 04.07.16
 * Time: 12:10
 */

namespace App\Http\Controllers;
use App\Advert;
use App\Good;
use Illuminate\Routing\Controller as BaseController;
use Yajra\Datatables\Facades\Datatables;


class GoodController extends BaseController
{

    public function getList(){
        // Using Eloquent
        return view('datatables.good');
    }

    public function getEditData($id = null){
        $data = Good::with('advert')->findOrFail($id);

        $adverts = call_user_func(function($res = []){
            foreach(Advert::all() as $advert){
                $res[$advert->user_id] = $advert->user_first_name.' '.$advert->user_last_name.' / '.$advert->user_login;
            }
            return $res;
        });

        return view('editforms.good', ['id' => $id, 'data' => $data, 'adverts' => $adverts]);
    }


    public function postSaveData($id){
        $good = Good::findOrFail($id);
        $good->fill(\Request::input());
        $good->save();
        return redirect(route('good.list'));
    }

    public function anyData()
    {
        $items = Good::with('advert')->get();
        return Datatables::of($items)
            ->editColumn('good_id', function($row) {
                return ' <input type="checkbox"> '.$row->good_id;
            })
            ->editColumn('good_name', function($row) {
                return '<a href="'.route('good.edit', ['id' => $row->good_id]).'">'. $row->good_name .'</a>';
            })
            ->editColumn('advert_user_name', function($row) {
                return '<div><a href="'.route('advert.edit', ['id' => $row->advert->user_id]).'">'.$row->advert->user_first_name.' '. $row->advert->user_last_name.'</a></div>'.
                    $row->advert->user_login;
            })
            ->make(true);
    }

}