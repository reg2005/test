<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {


    /*
     * Goods
     */

    Route::controller('good', 'GoodController', [
        'anyData'  => 'good.data',
        'getList'  => 'good.list',
        'getEditData'  => 'good.edit',
        'postSaveData'  => 'good.save'
    ]);

    /*
     * Orders
     */

    Route::controller('order', 'OrderController', [
        'anyData'  => 'order.data',
        'getList'  => 'order.list',
        'getEditData'  => 'order.edit',
    ]);

    /*
     * Adverts
     */

    Route::controller('advert', 'AdvertController', [
        'anyData'  => 'advert.data',
        'getList'  => 'advert.list',
        'getEditData'  => 'advert.edit'
    ]);

});




Route::auth();

Route::get('/home', 'HomeController@index');
