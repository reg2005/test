<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Order;
use App\State;
use App\Advert;
use App\Good;

class testData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,10) as $index) {
            Order::create([
                'order_state' => range(1,4),
                'order_good' => range(1,4),
                'order_client_phone' => $faker->phoneNumber,
                'order_client_name' => $faker->name,
            ]);
        }
    }
}
