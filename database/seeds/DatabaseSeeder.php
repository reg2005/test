<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Order;
use App\State;
use App\Advert;
use App\Good;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (!State::find(1)){
            State::create([
                'state_id' => 1,
                'state_name' => 'Новый',
                'state_slug' => 'new',
            ]);
        }

        if (!State::find(2)){
            State::create([
                'state_id' => 2,
                'state_name' => 'В работе',
                'state_slug' => 'onoperator',
            ]);
        }

        if (!State::find(3)){
            State::create([
                'state_id' => 3,
                'state_name' => 'Подтвержден',
                'state_slug' => 'accepted',
            ]);
        }

        $faker = Faker::create('ru_RU');

        foreach (range(4,40) as $index) {
            Advert::create([
                'user_first_name' => $faker->firstName,
                'user_last_name' => $faker->lastName,
                'user_login' => $faker->email,
                'user_password' => $faker->password,
            ]);
        }

        foreach (range(4,40) as $index) {
            Good::create([
                'good_name' => $faker->text(50),
                'good_price' => $faker->numberBetween(2000, 5000),
                'good_advert' => rand(1,3),
            ]);
        }

        foreach (range(3,40) as $index) {
            Order::create([
                'order_state' => rand(1,3),
                'order_good' => rand(1,3),
                'order_client_phone' => $faker->phoneNumber,
                'order_client_name' => $faker->name,
            ]);
        }
    }
}
