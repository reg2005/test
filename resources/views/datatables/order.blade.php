@extends('layouts.app')

@section('content')

    {!! Form::open(array('id' => 'search-form')) !!}
    {!! Form::label('from', 'От') !!}
    {!! Form::text('from', \Carbon\Carbon::now()->subDays(4), ['placeholder' => 'От'] ) !!}
    {!! Form::label('to', 'До') !!}
    {!! Form::text('to', \Carbon\Carbon::now()->addDays(5), ['placeholder' => 'До'] )!!}
    {!! Form::select('order_state', $states) !!}
    {!! Form::text('order_client_phone', NULL, ['placeholder' => 'Телефон']) !!}
    {!! Form::text('good_name', NULL, ['placeholder' => 'Товар']) !!}
    {!! Form::text('good_id', NULL, ['placeholder' => 'ID заказа']) !!}
    <button type="submit">Отправить</button>
    {!! Form::close() !!}

    <table class="table table-bordered" id="users-table">
        <thead>
        <tr>
            <th>#</th>
            <th>Дата</th>
            <th>Клиент</th>
            <th>Телефон</th>
            <th>Товар</th>
            <th>Статус</th>
        </tr>
        </thead>
    </table>
@stop

@push('scripts')
<script>
    $(function() {
        var oTable =$('#users-table').DataTable({
            processing: true,
            serverSide: true,
            language: {
                url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json'
            },
            ajax: {
                url: '{!! route('order.data') !!}',
                data: function (d) {
                    d.from = $('input[name=from]').val();
                    d.to = $('input[name=to]').val();
                    d.order_state = $('select[name=order_state]').val();
                    d.order_client_phone = $('input[name=order_client_phone]').val();
                    d.good_name = $('input[name=good_name]').val();
                    d.good_id = $('input[name=good_id]').val();
                }
            },

            columns: [
                { data: 'order_id', name: 'order_id' },
                { data: 'order_add_time', name: 'order_add_time' },
                { data: 'order_client_name', name: 'order_client_name'},
                { data: 'order_client_phone', name: 'order_client_phone' },
                { data: 'good', name: 'order_good' },
                { data: 'state', name: 'order_state' }
            ]
        });

        $('#search-form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
    });
</script>
@endpush