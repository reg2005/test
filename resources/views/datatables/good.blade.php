@extends('layouts.app')

@section('content')
    <table class="table table-bordered" id="users-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Название</th>
            <th>Цена</th>
            <th>Рекламодатель</th>
        </tr>
        </thead>
    </table>
@stop

@push('scripts')
<script>
    $(function() {
        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            language: {
                url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json'
            },
            ajax: '{!! route('good.data') !!}',
            columns: [
                { data: 'good_id', name: 'good_id' },
                { data: 'good_name', name: 'good_name'},
                { data: 'good_price', name: 'good_price' },
                { data: 'advert_user_name', name: 'good_advert' }
            ]
        });
    });
</script>
@endpush