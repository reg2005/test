@extends('layouts.app')

@section('content')

    {{ Form::model($data, array('route' => array('good.save', $data->good_id))) }}
    {{ Form::text('good_id') }}
    {{ Form::text('good_name') }}
    {{ Form::text('good_price') }}
    {{ Form::select('good_advert', $adverts) }}
    <button type="submit">Соранить</button>
@stop